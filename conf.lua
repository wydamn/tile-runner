-- Configuration 
function love.conf(t)
	t.title = "Tile Runner" -- The title of the window the game is in 
	t.version = "0.10.0" -- The LÖVE version this game was made for 
	t.window.width = 1000 -- we want our game to be long and thin. 
	t.window.height = 500 
	-- For Windows debugging 
	t.console = false
end
