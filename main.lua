debug = true
error_que = {}
draw_base_grid = false


--GLOBAL VARIABLES
  --GAME DATA
game_state = "INIT" -- possible states: INIT, MMENU, PMENU, RUN, SAVE, LOAD
game_profile = nil -- used for savefile names

  --OVERWORLD LAYER DATA
 
  --TACTICS LAYER DATA
level = nil --level object used for current level data
camera = nil --camera object used for level orientation and focus
tile_width = 80 --width from corner to corner of each tile at default zoom
default_tileset = { "normal", "long grass", "mud" } -- a simple, default tileset. More tilesets should be specified in xml.


function initialize()
	game_state = "INIT"
	game_profile = nil
	level = {
		name = "Untitled",
		size = 8,
		tileset = default_tileset,
		terrain = nil
	}
	camera = {
		dir = "NW",
		height = 0,
		pos = { x = love.graphics.getWidth()/2, y = love.graphics.getHeight()/2 },
		focus = { x = 0, y = 0 }, --start out focusing on tile 0,0, the topmost tile while dir is NW
		tilt = 0.45, -- also, rise over run
		drag_start = { x = 0, y = 0 }, -- store the mouse's initial position when clicking and dragging
		snapToFocus = true
	}
end

function love.load(arg)
--LOAD IMAGEMAPS
--LOAD AUDIO
	initialize()
	level.terrain = terrain_gen()
end

function love.update(dt)
-- CONTROLS
	-- In-Game Controls -- Q-west, W-north, S-east, A-south
--if (game_state == "RUNNING") then
	if love.keyboard.isDown('escape') then
		love.event.push('quit')
	end

	if love.keyboard.isDown('q') then
		rotate(true)
	end
	if love.keyboard.isDown('e') then
		rotate(false)
	end
	if love.keyboard.isDown('s') then
		setFocus(camera.focus.x + 1,camera.focus.y)
	end
	if love.keyboard.isDown('a') then
		setFocus(camera.focus.x, camera.focus.y + 1)
	end
	if love.keyboard.isDown('g') then
		if draw_base_grid then
			draw_base_grid = false
		else
			draw_base_grid = true
		end
	end
	if love.keyboard.isDown('t') then
		raiseNorthWest()
	end
	if love.keyboard.isDown('r') then
		level.terrain = terrain_gen(level.size)
	end

	-- Menu Controls
if (game_state == "MMENU" or "PMENU") then

end
-- PLAYER MOVEMENT
	-- impose speed limits
-- NPC MOVEMENT	
--


end

function terrain_gen(size) --generate a matrix to represent terrain.
	--TODO: Make sure unique levels are being generated each time.
	mt = {}
	for i = 0, level.size do
		mt[i] = {}
		for j = 0, level.size do
			mt[i][j] = math.random(0,3)
		end
	end
	return mt
end



function love.draw(dt)
	--SET levelmap position relative to camera position.
	pos = { x = 0, y = 0 }
	if mouseHeld then
		pos.x = camera.pos.x + love.mouse.getX() - camera.drag_start.x
		pos.y = camera.pos.y + love.mouse.getY() - camera.drag_start.y
	elseif camera.snapToFocus then
		xshift =  (camera.focus.x - camera.focus.y) * tile_width/2
		yshift = (camera.focus.y + camera.focus.x) * camera.tilt * tile_width/2
			+ camera.tilt * tile_width/2
		pos.x = love.graphics.getWidth()/2 - xshift
		pos.y = love.graphics.getHeight()/2 - yshift
		camera.snapToFocus = false
		camera.pos.x = pos.x
		camera.pos.y = pos.y
	else
		pos.x = camera.pos.x
		pos.y = camera.pos.y
	end

	origin = { x = pos.x, y = pos.y } --topmost corner of the grid, we begin drawing here.
	

-- Draw tiles and props starting with the top most tiles
	love.graphics.setLineStyle("smooth")
		-- TODO: Check level.terrain and level.size, make sure they both make sense.
	-- TODO: Check level.terrain value for each tile. adjust height and draw tile accordingly. Color tiles per height for bonus points.
	love.graphics.setColor(80,160,80,255)
	hS = 20 -- Height of each tile per step.
	for i = 0, level.size - 1 do
		for j = 0, level.size -1 do
			n = tile(i,j)
			htx = tile_width/2
			hty = tile_width*camera.tilt/2
			quadx = origin.x + i*htx - j*htx
			quady = origin.y + i*hty + j*hty - hS*n

			love.graphics.setColor(40 + (160 - n*40), 40 + n*40, 40, 255)
			love.graphics.polygon("fill",quadx - htx, quady + hty, quadx + htx, quady + hty, quadx + htx, quady + hty + hS*n, quadx, quady + 2*hty + hS*n, quadx - htx, quady + hty + hS*n)
			love.graphics.polygon("fill",quadx, quady,quadx + htx, quady + hty, quadx, quady + 2*hty, quadx - htx, quady + hty)
			
			love.graphics.setColor(255,255,255,225)
			love.graphics.line(quadx, quady, quadx + htx, quady + hty)
			love.graphics.line(quadx + htx, quady + hty, quadx, quady + 2*hty)
			love.graphics.line(quadx, quady + 2*hty, quadx - htx, quady +hty)
			love.graphics.line(quadx - htx, quady + hty, quadx, quady)


		end
	end

	--DRAW CROSSHAIR
	love.graphics.setColor(255,0,0,255)
	crossSize = tile_width * 0.75
	ac = love.graphics.getWidth()/2
	bc = love.graphics.getHeight()/2
	love.graphics.line(ac - crossSize, bc, ac + crossSize, bc)
	love.graphics.line(ac, bc - crossSize, ac, bc + crossSize)
	love.graphics.setColor(255,255,255,255)
	
	love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 12)
	love.graphics.print("Origin Position: x "..camera.pos.x..", y "..camera.pos.y, 10, 24)
	love.graphics.print("Dragging by: x "..camera.drag_start.x..", y "..camera.drag_start.y..".", 10, 36)
	love.graphics.print("Focus: x "..camera.focus.x..", y "..camera.focus.y, 10, 48)
	love.graphics.print("Camera direction is "..camera.dir, 10, 60)

	--DRAW THE GRID
	if draw_base_grid then
		love.graphics.setColor(200,200,200,120)
		for i = 0, level.size do 
			posXA = origin.x - i * tile_width/2
			posXB = origin.x + i * tile_width/2
			posY = origin.y + i * tile_width * camera.tilt/2
	
			love.graphics.line(posXA, posY, posXA + tile_width/2 * level.size, posY + tile_width/2 * camera.tilt * level.size)
			love.graphics.line(posXB, posY, posXB - tile_width/2 * level.size, posY + tile_width/2 * camera.tilt * level.size)
		end
	end


end

function tile(xDraw,yDraw) --translate correct tile at drawing position /w camera.dir
	if camera.dir == "NW" then
		return level.terrain[xDraw][yDraw]
	elseif camera.dir == "NE" then
		return level.terrain[level.size - 1- yDraw][xDraw]
	elseif camera.dir == "SW" then
		return level.terrain[yDraw][level.size - 1 - xDraw]
	elseif camera.dir == "SE" then
		return level.terrain[level.size - 1 - xDraw][level.size - 1 - yDraw]
	else
		print("CAMERA ERROR; invalid direction")
	end
end

function rotate(clockwise)
	if camera.dir == "NW" then
		if clockwise then
			camera.dir = "NE"
		else
			camera.dir = "SW"
		end
	elseif camera.dir == "NE" then
		if clockwise then
			camera.dir = "SE"
		else
			camera.dir = "NW"
		end
	elseif camera.dir == "SE" then
		if clockwise then
			camera.dir = "SW"
		else
			camera.dir = "NE"
		end
	elseif camera.dir == "SW" then
		if clockwise then
			camera.dir = "NW"
		else
			camera.dir = "SE"
		end
	else
		print("ERROR: tried to rotate camera with invalid direction")
	end
end


function setFocus(x, y)
	camera.snapToFocus = true
	camera.focus.x = x
	camera.focus.y = y
end


function raiseNorthWest()
	for i = 0, level.size/3 do
		level.terrain[i][0] = 4
		level.terrain[0][i] = 4
	end
end

function love.mousepressed(x, y, button, isTouch)
	mouseHeld = true
	camera.drag_start = { x = love.mouse.getX(), y = love.mouse.getY() }
end

function printErr() --call this with love.draw --WAIT, REALLY???
	--TODO: Log errors?
--	love.graphics.setColor(255,0,0,0)
--	if error_que.length = 0 then  --TODO: figure out correct length function for lua
--		love.graphics.print("Attempted to print errors, que empty", 10, love.graphics.getHeight())
--	elseif error_que.length = 1 then
--		love.graphics.print(error_que[0], 10, love.graphics.getHeight())
--	elseif error_que.length > 1 then
--		for i, errorstring in ipairs(error_que), 1 do 
--			love.graphics.print(error_string, 10, love.graphics.getHeight() + i * 10)
--		end
--	end

end

function addErr(error_string)

	--add error to error table/queue
	manageErr()
end

function manageErr()
	--organize error_queue, counting recurring errors and discarding errors after
	--a certain amount of time. Called by both addErr() and by love.update()
	--
	--Use a table?
end

function love.mousereleased(x,y,button, isTouch)
	mouseHeld = false

	camera.pos.x = camera.pos.x + love.mouse.getX() - camera.drag_start.x
	camera.pos.y = camera.pos.y + love.mouse.getY() - camera.drag_start.y
	pos.y = camera.pos.y + love.mouse.getY() - camera.drag_start.y

	pos.y = camera.pos.y + love.mouse.getY() - camera.drag_start.y
end


-- Map and Tile functions

function newTile()
	return newTile(0,0)
end

function newTile(height, tileType)
	if tileType < level.tileset.getLength() then -- TODO: Find the right func for finding the size/length of an array.
		tile = { height, tileType }   -- decide on tile types. Might depend on tileset.
	else
		print("ERROR: Attempted to create an invalid tile "..tileType.." in a tileset containing "..tileset.getLength())
	end
end
